using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlGrue : MonoBehaviour
{
    private List<Camera> cameras;
    private int currentCameraIndex;
    public float torque = 250f;
    public float forceChariot = 500f;
    public float forceMoufle = 500f;
    public ArticulationBody pivot;
    public ArticulationBody chariot;
    public ArticulationBody moufle;
    // Start is called before the first frame update
    void Start()
    {
        cameras = new List<Camera>(FindObjectsOfType<Camera>());
        currentCameraIndex = 3;
        for(int i=0;i<cameras.Count;i++)
        {
            if(i == currentCameraIndex)
            {
                cameras[i].enabled = true;
            }
            else
            {
                cameras[i].enabled = false;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Commande pour la flèche
        if(Input.GetKey(KeyCode.LeftArrow)){
            pivot.AddTorque(transform.up * -torque);
        }
        if(Input.GetKey(KeyCode.RightArrow)){
            pivot.AddTorque(transform.up * torque);
        }
        //Commande pour le chariot
        if(Input.GetKey(KeyCode.UpArrow)){
            chariot.AddRelativeForce(transform.right * forceChariot);
        }
        if(Input.GetKey(KeyCode.DownArrow)){
            chariot.AddRelativeForce(transform.right * -forceChariot);
        }
        //Commande pour la moufle
        if(Input.GetKey(KeyCode.LeftShift)){
            moufle.AddRelativeForce(transform.up * forceMoufle);
        }
        if(Input.GetKey(KeyCode.LeftControl)){
            moufle.AddRelativeForce(transform.up * -forceMoufle);
        }
        //Commande pour changer de caméra
        if (Input.GetKeyDown(KeyCode.V))
        {
            cameras[currentCameraIndex].enabled = false;
            currentCameraIndex++;
            if (currentCameraIndex == cameras.Count)
            {
                currentCameraIndex = 0;
            }
            cameras[currentCameraIndex].enabled = true;
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            cameras[currentCameraIndex].enabled = false;
            currentCameraIndex--;
            if (currentCameraIndex == -1)
            {
                currentCameraIndex = cameras.Count-1;
            }
            cameras[currentCameraIndex].enabled = true;
        }


    }
}
